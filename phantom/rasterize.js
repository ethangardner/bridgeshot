var page = require('webpage').create(),
    system = require('system'),
    address, output, size;

if (system.args.length < 3 || system.args.length > 5) {
    console.log('Usage: rasterize.js URL filename [width*height] [zoom]');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    size = [];

    if (system.args.length > 3) {
        size = system.args[3].split('*');
    } else {
        size[0] = 1024;
        size[1] = 768;
    }

    page.viewportSize = { width: size[0], height: size[1] };

    if (system.args.length > 4) {
        page.zoomFactor = system.args[4];
    }
    page.open(address, function (status) {
        if (status !== 'success') {
            console.log('Unable to load the address!');
            phantom.exit();
        } else {
            window.setTimeout(function () {
                page.render(output);
                phantom.exit();
            }, 200);
        }
    });
}
