This is a python script to recursively call a phantomjs script recursively
against a list of urls that are defined in a text file (one per line).

In order to use it, you will need [phantomjs](http://phantomjs.org/)
and [Python 2.7](http://www.python.org/) installed on your system path
so they are accessible via command line.

## Options
`-i`, `--input` : path to a text file containing one URL per line
`-o`, `--output` : output directory for images
`-s`, `--size` : the minimum output size of the images formatted as width*height (e.g. 1024*768, 320*480, etc.). Defaults to 1024*768 if no value is specified.
