#!/usr/bin/env python
# encoding: utf-8
import argparse
import os
import subprocess

class Shoot():
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-i, --input', action='store', dest='input', required=True, help='Input - can be url or text file containing one URL per line')
        parser.add_argument('-o, --output', action='store', dest='dir', required=True, help='Output directory for images')
        parser.add_argument('-s, --size', action='store', dest='size', default='1024*768', help='output size for images \(width*height\) - ex. 1024*768')
        opts = parser.parse_args()

        self.setup(opts)
        self.takeShots(opts)

    def setup(self, opts):
        if not os.path.exists(opts.dir):
            os.makedirs(opts.dir)

    def getUrls(self, opts):
        #open a text file
        lines = [line.strip() for line in open(opts.input)]
        return lines

    def takeShots(self, opts):
        #take screenshots
        basepath = os.path.dirname(os.path.realpath(__file__)) + os.sep
        phantom = basepath + 'phantom' + os.sep
        outputdir = opts.dir + os.sep
        urls = self.getUrls(opts)
        i = 1

        for url in urls:
            try:
                subprocess.Popen(['phantomjs', phantom + 'rasterize.js', url, outputdir + str(i) + '-webkit.png', opts.size], stdout=subprocess.PIPE)
            except Exception:
                print "PhantomJS - error getting " + url

            i = i + 1


if __name__ == '__main__':
    go = Shoot()
